/*jshint esversion: 6*/

/*
 * Globals.
 */

var igtparams = [];
var testfilter = "";
var fontheight = 11;
var testboxheight = 18;
var maxtestpercanvas = Math.ceil(1000/testboxheight);
var defaultURL = "";

var ttip = {self:null, text: "", timeout:0, shown:false};

var loadedfiles = {};

/*
 * legend graphic images
 */
var warn_image = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAjwCPAAD/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCAAeABsDASIAAhEBAxEB/8QAGAAAAwEBAAAAAAAAAAAAAAAAAAEEAgX/xAAbEAADAQEBAQEAAAAAAAAAAAAAAQIRAyExYf/EABgBAAMBAQAAAAAAAAAAAAAAAAABBgID/8QAFxEBAQEBAAAAAAAAAAAAAAAAAAIRAf/aAAwDAQACEQMRAD8A6IAY6WoXpKqAuvRQv0lb16x1Tp6xHWeYSy6UTrJLt3Wsd27fvwyKZwAAA2H/2Q==";
var warn_img; // actual image which is used.
var skip_image = "data:image/gif;base64,R0lGODdhGwAeAIACAICAgKCgoCwAAAAAGwAeAAACSoyPqRvggB48ks5qXXzWYM9lUzMy5omm6sq2rvmRmtjJZfzNeAjO5AsMCofE1c5nqx1vPB0vSQMVp9SqdbHc+JzIrBTaY25LV2EBADs=";
var skip_img;
var fail_image = "data:image/gif;base64,R0lGODlhGwAeAKUwALMAALUAALYAALcAALgAALkAALoAALsAALwAAL0AAL8BAMACAcMDAcgFAskFAsoFAssGAswGAs0GAs4HAtIJA9cKBNgLBNkLBNoLBNoMBN8NBeEOBeMPBegRBuwTB+0TB+8UB/AUB/EVB/IVB/QWCPcXCPgXCPkYCPsYCP0ZCf4aCf8aCf8bCf8bCv8cCv8dCv///////////////////////////////////////////////////////////////yH5BAEKAD8ALAAAAAAbAB4AAAaqwNRqSCwaj0aBB8lsrhCAjstJHSIKFhOr6kQgBI/Plov0ficvctl7WGCm6qIZcUBAVuP4nL0Y5cl7XgUjcGqBXgIcf1WHXgEVhYyNBBQni02NXgMMIICZVwotXJ90Chd4VKSCESSXcqoIAyKRR7BeCR1pTLZeBBa0RLy9Dq7CVw4oa8IGByEqRsZsAiV/0WwZKKJW1rESIGPcjhu64aUaaeV0BxMs6VcNLEEAOw==";
var fail_img;

function checkenter(e) {
    if(event.key === 'Enter') {
        for (var i = 0; i < igtparams.length; i +=5)
            drawResults(igtparams[i], igtparams[i+1], igtparams[i+2], igtparams[i+3], igtparams[i+4]);
    }
}

var readFile = function (file) {
    return new Promise((resolve, reject) => {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.responseType = "arraybuffer";

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState !== 4)
                return;

            if (xmlhttp.status >= 200 && xmlhttp.status < 300) {
                var decoder = new TextDecoder();
                resolve(JSON.parse(decoder.decode(pako.inflate(xmlhttp.response))));
            } else {
                reject({
                    status: xmlhttp.status,
                    statusText: xmlhttp.statusText
                });
            }
        };

        xmlhttp.ontimeout = function () {
            console.log('timeout');
            reject('timeout')
        };

        xmlhttp.open('GET', file, true);
        xmlhttp.send();
    });
}

async function fetchfile(file) {
    try {
        if (loadedfiles[file] === undefined)
            loadedfiles[file] = {json:await readFile(file)};
    } catch(err) {
        console.log("fetching file " + file + " failed.\n" + err.status + "\n" + err.statusText);
    }
}

function getmaxtesttextwidth(ctx, measureitem, usefilter, file) {
    var maxlen = 0;
    var usedfilter = "";
    if (usefilter === true) {
        loadedfiles[file].testlist_filtered = [];

        try {
            new RegExp(testfilter).test("regexp test to see its all ok.")
            usedfilter = testfilter;
        } catch(e) {}
    }

    for (var i = 0; i < measureitem.length; i++) {
        if (!usefilter || (usedfilter === "" || new RegExp(usedfilter).test(measureitem[i]))) {
            if (usefilter)
                loadedfiles[file].testlist_filtered.push(i);

            var thistextlenght = ctx.measureText(measureitem[i]).width;
            if (thistextlenght > maxlen)
                maxlen = thistextlenght;
        }
    }
    return maxlen;
}

/*
 * draw headers onto canvas
 */
function drawheaders(ctx, file, group_by_run) {
    var l1, l2;

    ref = loadedfiles[file];
    ctx.translate(ref.maxtesttextwidth, 0);
    ctx.strokeStyle = "black";
    ctx.lineWidth = 1;
    ctx.fillStyle = "blue";
    ctx.font = fontheight + "px Arial";
    ctx.textAlign = "center";

    if (group_by_run === false) {
        l1 = ref.json.index.hosts;
        l2 = ref.json.index.builds;
    } else {
        l2 = ref.json.index.hosts;
        l1 = ref.json.index.builds;
    }

    for (var i = 0; i < l1.length; i++) {
        ctx.strokeRect(0, 0, ref.hosttextwidth - 1, 24 - 1);
        ctx.fillText(l1[i], ref.hosttextwidth / 2, 16);

        for (var j = 0; j < l2.length; j++) {
            ctx.save();
            ctx.translate(j*ref.buildtextboxheight, 24);
            if (j === 0) {
                ctx.beginPath();
                ctx.moveTo(0, 0);
                ctx.lineTo(0, ref.buildtextwidth);
                ctx.stroke();
            }

            if (j === l2.length - 1) {
                ctx.beginPath();
                ctx.moveTo(ref.buildtextboxheight - 1, 0);
                ctx.lineTo(ref.buildtextboxheight - 1, ref.buildtextwidth);
                ctx.stroke();
            }

            ctx.rotate(Math.PI / 2);
            ctx.fillText(l2[j], ref.buildtextwidth / 2, -(ref.buildtextboxheight - fontheight) / 2);
            ctx.restore();
        }
        ctx.translate(ref.hosttextwidth, 0);
    }
}

function tooltipf(canvas, div, text, file) {
    this.show = function(e) {
        ttip.shown = true;
        div.style.left = e.clientX + "px";
        div.style.top = e.clientY + "px";
        document.body.appendChild(div);
        ttip.timeout = setTimeout(this.hide, 3000);
    }

    this.hide = function() {
        ttip.shown = false;
        try {
            document.body.removeChild(div);
        }
        catch (err) {}

        ttip.self = null;
    }

    function startshow(e) {
        if (!ttip.shown && ttip.self !== null && e.clientX - canvas.getBoundingClientRect().left >= loadedfiles[file].maxtesttextwidth)
            ttip.self.show(e);
    }

    if (ttip.text !== text && ttip.self !== null) {
        clearTimeout(ttip.timeout);
        ttip.self.hide();
    }

    if (ttip.self !== null)
        return;

    ttip.self = this;
    ttip.shown = false;
    ttip.text = text;

    div.style.cssText = "font-family: monospace;color:black;border:2px solid black;font-size:0.8em;position:fixed;padding:7px;background:white repeat-x 0 0;pointer-events:none;";
    canvas.addEventListener("mousemove", startshow);
}

function drawtests(appendafter, file, group_by_run) {
    return new Promise((resolve, reject) => {
        var ref = loadedfiles[file];
        var index = ref.json.index;
        var teststorender = ref.testlist_filtered.length;
        var ctx;
        var l1, l2;

        if (group_by_run === false) {
            l1 = index.hosts;
            l2 = index.builds;
        } else {
            l2 = index.hosts;
            l1 = index.builds;
        }

        var legendary = {
            pass: { color:["#30ff30", "#20e820"] },
            skip: { renderimg:skip_img },
            notrun: { color:["#e0e0e0"] },
            dmesg_warn: { renderimg:warn_img, extrachar:'D' },
            warn: { renderimg:warn_img },
            dmesg_fail: { renderimg:fail_img, extrachar:'D' },
            fail: { renderimg:fail_img },
            timeout: { color:["#83bdf6"], extrachar:'T' },
            abort: { color:["#111111"] },
            crash: { color:["#111111"] },
            trap: { color:["#111111"] },
            incomplete: { color:["#853385"] }
        };

        var i = 0;
        const intervalId = setInterval(() => {
            for (var a = 0; i < teststorender && a < 100; i++, a++) {
                var rendertest = index.tests[ref.testlist_filtered[i]];

                /*
                 * need to set up new canvas
                 */
                if ((i%maxtestpercanvas) === 0) {
                    var x = document.createElement("CANVAS");
                    var linestodraw = maxtestpercanvas;
                    ctx = x.getContext("2d");
                    const dpr = window.devicePixelRatio;
                    ctx.font = fontheight + "px Arial";

                    if (teststorender - i < maxtestpercanvas)
                        linestodraw = teststorender - i;

                    contentwidth = l1.length * ref.hosttextwidth + ref.maxtesttextwidth;
                    contenheight = linestodraw * testboxheight;
                    x.width = Math.ceil(dpr * contentwidth);
                    x.height = Math.ceil(dpr * contenheight);

                    x.style.width = contentwidth + "px";
                    x.style.height = contenheight + "px";
                    ctx.scale(dpr, dpr);

                    /*
                     * each canvas get mouse listener for tooltips and
                     * updating which test is being pointed.
                     */
                    x.onmousemove = function(e) {
                        ref.onclicklink = "";
                        this.style.cursor = 'default';
                        var rect = this.getBoundingClientRect(),
                        x = e.clientX - rect.left,
                        y = e.clientY - rect.top;
                        for (var n = 0; n < ref.canvaslist.length; n++) {
                            if (this === ref.canvaslist[n]) {
                                pointingtest = index.tests[ref.testlist_filtered[Math.floor((n - 1) * maxtestpercanvas + y / testboxheight)]];

                                if (x < ref.maxtesttextwidth) {
                                    this.style.cursor = 'pointer';
                                    ref.onclicklink = defaultURL + pointingtest + ".html";
                                } else {
                                    if (group_by_run === false) {
                                        var host = l1[Math.floor((x - ref.maxtesttextwidth) / ref.hosttextwidth)];
                                        var build = l2[Math.floor(((x - ref.maxtesttextwidth) % ref.hosttextwidth) / ref.buildtextboxheight)];
                                    } else {
                                        var build = l1[Math.floor((x - ref.maxtesttextwidth) / ref.hosttextwidth)];
                                        var host = l2[Math.floor(((x - ref.maxtesttextwidth) % ref.hosttextwidth) / ref.buildtextboxheight)];
                                    }
                                    hoverkey = build + "," + host + "," + pointingtest;

                                    var a = document.createElement("a");
                                    var div = document.createElement("div");
                                    var val = ref.json.data[hoverkey];
                                    var result = val ? val.result : "notrun"

                                    if (result != "pass" && result != "notrun") {
                                        if (val.hostname)
                                            ref.onclicklink = defaultURL + build + "/" + val.hostname + "/" + pointingtest + ".html";
                                        else
                                            ref.onclicklink = defaultURL + build + "/" + host + "/" + pointingtest + ".html";

                                        var b = document.createElement("b");
                                        b.appendChild(document.createTextNode(build + " / " + host));
                                        b.appendChild(document.createElement("br"));
                                        b.appendChild(document.createTextNode(pointingtest));
                                        b.appendChild(document.createElement("br"));

                                        var span = document.createElement("span");
                                        span.appendChild(b);

                                        if (val.tooltip) {
                                            var pre = document.createElement("pre");
                                            pre.innerHTML = val.tooltip;
                                            span.appendChild(pre);
                                        }
                                        div.appendChild(span);
                                    }
                                    div.appendChild(a);

                                    if (result !== "pass" && result !== "notrun") {
                                        this.style.cursor = 'pointer';
                                        var t1 = new tooltipf(this, div, hoverkey, file);
                                    } else {
                                        if (ttip.self !== null ) {
                                            clearTimeout(ttip.timeout);
                                            ttip.self.hide();
                                        }
                                    }
                                }
                            }
                        }
                    };
                    x.onclick = function () {
                        if (ref.onclicklink === "")
                            return;

                        if (window.event.ctrlKey || window.event.metaKey) {
                            window.open(ref.onclicklink);
                            window.focus();
                            return false;
                        } else {
                            document.location = ref.onclicklink;
                        }
                    };
                    document.getElementById(appendafter).appendChild(x);
                    ref.canvaslist.push(x);
                }

                /*
                 * Start to draw one test line
                 */
                ctx.beginPath();
                ctx.moveTo(0, 1);
                ctx.lineTo(ref.maxtesttextwidth, 1);
                ctx.stroke();

                ctx.fillStyle = "blue";
                ctx.fillText(rendertest, 0, fontheight + 1);

                for (var j = 0; j < l1.length; j++) {
                    for (var k = 0; k < l2.length; k++) {
                        if (group_by_run === false)
                            key = l2[k] + "," + l1[j] + "," + index.tests[ref.testlist_filtered[i]];
                        else
                            key = l1[j] + "," + l2[k] + "," + index.tests[ref.testlist_filtered[i]];

                        var val = ref.json.data[key];
                        var result = val ? val.result.replace(/-/g, '_') : "notrun"

                        var x_help = j * l2.length + k;

                        if (legendary[result].renderimg !== undefined) {
                            ctx.drawImage(legendary[result].renderimg, ref.maxtesttextwidth + x_help * ref.buildtextboxheight, 1, ref.buildtextboxheight-2, testboxheight - 2);
                        } else {
                            ctx.fillStyle = legendary[result].color[i % legendary[result].color.length];
                            ctx.fillRect(ref.maxtesttextwidth + x_help * ref.buildtextboxheight, 1, ref.buildtextboxheight-2, testboxheight - 2);
                        }

                        if (legendary[result].extrachar !== undefined) {
                            ctx.fillStyle = "blue";
                            ctx.fillText(legendary[result].extrachar, ref.maxtesttextwidth + x_help * ref.buildtextboxheight + 2, fontheight + 1);
                        }

                        if (k === 0) {
                            ctx.beginPath();
                            ctx.moveTo(ref.maxtesttextwidth + x_help * ref.buildtextboxheight, 0);
                            ctx.lineTo(ref.maxtesttextwidth + x_help * ref.buildtextboxheight, testboxheight + 4);
                            ctx.stroke();
                        }
                        if (k === l2.length - 1) {
                            ctx.beginPath();
                            ctx.moveTo((ref.maxtesttextwidth + (x_help + 1) * ref.buildtextboxheight) - 1, 0);
                            ctx.lineTo((ref.maxtesttextwidth + (x_help + 1) * ref.buildtextboxheight) - 1, testboxheight + 4);
                            ctx.stroke();
                        }
                    }
                }
                ctx.translate(0, testboxheight);
            }
            if (i >= teststorender) {
                clearInterval(intervalId);
                resolve(true);
            }
        }, 0);
    });
}

async function drawResults(file, target_div, spinner, group_by_run, depth) {
    var filtertext = document.getElementById("filtertext");

    if (filtertext !== null)
        filtertext.disabled = true;

    if (!(warn_img instanceof HTMLImageElement)) {
        warn_img = new Image;
        warn_img.src = warn_image;
        skip_img = new Image;
        skip_img.src = skip_image;
        fail_img = new Image;
        fail_img.src = fail_image;

        if (depth !== undefined)
            defaultURL = "../".repeat(depth);
    }

    if (!igtparams.includes(file)) {
        igtparams.push(file);
        igtparams.push(target_div);
        igtparams.push(spinner);
        igtparams.push(group_by_run);
        igtparams.push(depth);
    } else {
        if (loadedfiles[file] != undefined ) {
            for (var i = 0; i < loadedfiles[file].canvaslist.length; i++) {
                loadedfiles[file].canvaslist[i].parentNode.removeChild(loadedfiles[file].canvaslist[i]);
            }
        }
    }

    if (ttip.self !== null) {
        clearTimeout(ttip.timeout);
        ttip.self.hide();
        ttip.self = null;
        ttip.shown = false;
        ttip.text = "";
    }

    if (filtertext !== null)
        testfilter = document.getElementById("filtertext").value;
    else
        testfilter = "";

    await fetchfile(file);

    var ref = loadedfiles[file];

    var delayanim = document.getElementById(spinner);
    if (delayanim)
        delayanim.parentNode.removeChild(delayanim);

    if (ref === undefined) {
        var txt = document.createTextNode("Couldn't fetch results, testing still in progress?");
        document.getElementById(target_div).appendChild(txt);
        if (filtertext !== null)
            filtertext.disabled = false;
        return;
    }

    var x = document.createElement("CANVAS");
    var ctx = x.getContext("2d");
    const dpr = window.devicePixelRatio;
    ctx.font = fontheight + "px Arial";

    var l1, l2;

    if (group_by_run === false) {
        l1 = ref.json.index.hosts;
        l2 = ref.json.index.builds;
    } else {
        l2 = ref.json.index.hosts;
        l1 = ref.json.index.builds;
    }

    ref.hosttextwidth = getmaxtesttextwidth(ctx, l1, false, file) + 4;
    if (l2.length * fontheight > ref.hosttextwidth)
        ref.hosttextwidth = l2.length * (fontheight + 4);

    ref.buildtextboxheight = ref.hosttextwidth / l2.length;
    ref.buildtextwidth = getmaxtesttextwidth(ctx, l2, false, file) + 4;
    ref.maxtesttextwidth = getmaxtesttextwidth(ctx, ref.json.index.tests, true, file);

    contentwidth = l1.length * ref.hosttextwidth + ref.maxtesttextwidth;
    contenheight = ref.buildtextwidth + 24;
    x.width = Math.ceil(dpr * contentwidth);
    x.height = Math.ceil(dpr * contenheight);

    x.style.width = contentwidth + "px";
    x.style.height = contenheight + "px";
    ctx.scale(dpr, dpr);

    ref.maxtesttextwidth = getmaxtesttextwidth(ctx, ref.json.index.tests, true, file) + 5;

    x.onmousemove = function(e) {
        ref.onclicklink = "";
        var rect = this.getBoundingClientRect(),
        x = e.clientX - rect.left,
        y = e.clientY - rect.top;
        if (x < ref.maxtesttextwidth) {
            this.style.cursor = 'default';
            return;
        } else {
            this.style.cursor = 'pointer';
            var n = Math.floor((x - ref.maxtesttextwidth) / ref.hosttextwidth);
            if (y <= 24) {
                ref.onclicklink = defaultURL + l1[n] + ((group_by_run === true) ? "/" : ".html");
            } else {
                var m = Math.floor(((x - ref.maxtesttextwidth) / ref.buildtextboxheight)%l2.length);
                ref.onclicklink = defaultURL + l2[m] + ((group_by_run === true) ? ".html" : "/");
            }
        }
    };
    x.onclick = function () {
        if (ref.onclicklink === "")
            return;

        if (window.event.ctrlKey || window.event.metaKey) {
            window.open(ref.onclicklink);
            window.focus();
            return false;
        } else {
            document.location = ref.onclicklink;
        }
    };

    drawheaders(ctx, file, group_by_run);

    document.getElementById(target_div).appendChild(x);
    ref.canvaslist = [];
    ref.canvaslist.push(x);
    if (ref.testlist_filtered.length > 0) {
        await drawtests(target_div, file, group_by_run);
    } else {
        var x = document.createElement("CANVAS");
        var ctx = x.getContext("2d");
        x.width = Math.ceil(dpr * contentwidth);
        x.height = Math.ceil(dpr * contenheight);

        x.style.width = contentwidth + "px";
        x.style.height = contenheight + "px";
        ctx.scale(dpr, dpr);

        ctx.font = "30px Arial";
        ctx.fillStyle = "black";
        ctx.fillText("None of the tests matched your filter!", 0, 40);
        document.getElementById(target_div).appendChild(x);
        ref.canvaslist.push(x);
    }
    if (filtertext !== null)
        filtertext.disabled = false;
}
