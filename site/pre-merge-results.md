# Pre-Merge Results

A handy overview of the emails with results sent by i915 CI.

## The Sections

### Test Results

 * **Possible new issues** - issues that are not [tracked by
   cibulog](/#results-filtering-and-bug-tracking) and seem to be caused by the
   patch series, if present the result is failure
 * **Known issues** - issues that occurred during the run but are known and
   tracked, they do not affect the result for the series
 * **Possible fixes** - issues that occurred in the base run (post-merge that
   serves as a baseline for comparison) but were not seen in the pre-merge
   run for the series
 * **Suppressed** - content that is not affecting the end result because the
   machine/configuration is either too new or too unstable, they are still
   listed for completeness

See [Understanding The Test Results](#understanding-the-test-results) for
details on how to read failures/fixes.


### Participating Hosts

This sections how many hosts participated in the run and the delta in
comparison to the base run.

**Small fluctuations are expected**, as we deal with rebooting and AC-cutting
physical machines - some of the host require manual intervention from time to
time or may take longer than expected to boot due to automated fscks.

When assessing impact of series look for **large deltas** and/or **patterns**
(e.g. half of the machines missing, or all GEN8 machines missing).

    Participating hosts (52 -> 47)
    ------------------------------

      Additional (2): fi-byt-j1900 fi-bsw-kefka
      Missing    (7): fi-ilk-m540 fi-hsw-4200u fi-byt-squawks fi-bsw-cyan fi-ctg-p8600 fi-byt-clapper fi-bdw-samus 


### Build Changes

This section lists the delta in components deployed on the machines doing
the testing.

You should see only one component change at a time (e.g. post-merge kernel
changed to the pre-merge) unless you are testing [combined IGT and kernel
changes](/test-with.html).

| Prefix      | Explanation                                                    |
| --          | --                                                             |
| CI\_DRM\_   | post-merge of drm-tip serving as a baseline for comparison     |
| Patchwork\_ | pre-merge of drm-tip with patches applied                      |
| Trybot\_    | pre-merge of drm-tip with patches applied ([trybot](/#trybot)) |
| IGT\_       | post-merge of IGT serving as a baseline for comparison         |
| IGTPW\_     | pre-merge of IGT with patches applied                          |
| TrybotIGT\_ | pre-merge of drm-tip with patches applied ([trybot](/#trybot)) |

    Build changes
    -------------

      * Linux: CI_DRM_6039 -> Patchwork_12960

      CI_DRM_6039: 941848427de77537b5709bd68ca4a4048be5b5d9 @ git://anongit.freedesktop.org/gfx-ci/linux
      IGT_4972: f052e49a43cc9704ea5f240df15dd9d3dfed68ab @ git://anongit.freedesktop.org/xorg/app/intel-gpu-tools
      Patchwork_12960: 824b9e2304e344e55b1fea49f624c7b2ab0725bf @ git://anongit.freedesktop.org/gfx-ci/linux

See [Reproducing The Tree](#reproducing-the-tree) on details how to read
the SHA1/repos.


## Understanding The Test Results

    * igt@i915_suspend@debugfs-reader:
     - shard-apl:          [PASS][5] -> [DMESG-WARN][6] ([fdo#108566]) +3 similar issues
     [5]: https://intel-gfx-ci.01.org/tree/drm-tip/CI_DRM_6039/shard-apl5/igt@i915_suspend@debugfs-reader.html
     [6]: https://intel-gfx-ci.01.org/tree/drm-tip/Patchwork_12960/shard-apl5/igt@i915_suspend@debugfs-reader.html

    [fdo#108566]: https://bugs.freedesktop.org/show_bug.cgi?id=108566

Each test state change (e.g. pass→fail) has few things listed:

 * **affected test:** e.g. `igt@i915_suspend@debugfs-reader`
 * **affected machine(s):** e.g. `shard-apl`
 * **state tranision:**: `[PASS][5] -> [DMESG-WARN][6]`
 * **linked result details:** e.g.: `[5]: https://intel-gfx-ci.01.org/tree/drm-tip/CI_DRM_6039/shard-apl5/igt@i915_suspend@debugfs-reader.html`
 * **associated bugs:** `([fdo#108566])`
 * **numer of occurences of same transition across multiple machines:** `+3 similar issues`


## I've Got Some False Positives, What Now?

Reply to the e-mail with the results and explain why do those results are
false positive in as much detail as you can.

Include the following people in the CC:

 * `"Martin Peres" <martin.peres@linux.intel.com>`
 * `"Lakshminarayana Vudum" <lakshminarayana.vudum@intel.com>`


## Reproducing The Tree

The easiest way is using ["build changes"](#build-changes) section.

    Build changes
    -------------

        * Linux: CI_DRM_5951 -> Patchwork_12825

      CI_DRM_5951: 08cf1213114f21fc0b1a17eac081a2db8c03311f @ git://anongit.freedesktop.org/gfx-ci/linux
      IGT_4956: 1d921615b0b706f25c856aa0eb096f274380c199 @ git://anongit.freedesktop.org/xorg/app/intel-gpu-tools
      Patchwork_12825: f3e8eb0c781a6a88cd6f67a64f4b93e132cb37fe @ git://anongit.freedesktop.org/gfx-ci/linux
      piglit_4509: fdc5a4ca11124ab8413c7988896eec4c97336694 @ git://anongit.freedesktop.org/piglit

Each component here has three things mentioned: it's name (which is also a
tag), SHA1 and git remote where given SHA1 and/or tag can be found.

    $ git remote -v
    ci-tags git://anongit.freedesktop.org/gfx-ci/linux (fetch)
    ci-tags git://anongit.freedesktop.org/gfx-ci/linux (push)

    $ git fetch --tags ci-tags

    $ git tag | grep Patchwork_12825
    intel/Patchwork_12825

    $ git checkout intel/Patchwork_12825
    Note: checking out 'intel/Patchwork_12825'.

    You are in 'detached HEAD' state. You can look around, make experimental
    changes and commit them, and you can discard any commits you make in this
    state without impacting any branches by performing another checkout.

    If you want to create a new branch to retain commits you create, you may
    do so (now or later) by using -b with the checkout command again. Example:

      git checkout -b <new-branch-name>

    HEAD is now at f3e8eb0c781a drm/i915: Add transcoder parameter to PSR registers macros

For the Kernel, if you want to check which upstream relase the tree is based
on you can fetch Linus' tags and use `git describe` to find the parent
annotated tag (CI tags are not annotaded).

    $ git remote add linus git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git

    $ git fetch --tags linus

    $ git describe intel/Patchwork_12825
    v5.1-rc5-1414-gf3e8eb0c781a

**NOTE:** IGT pre-merge tagging is still WIP. For now use SHA1 on the main
          repository. In the meantime checkout the IGT repository to SHA1
	  mentioned by the `IGT_` component and apply the mbox downloaded
	  from the patchwork.
